using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Stiply.Sdk.Tests
{
    [TestClass]
    public class StiplyClientTest
    {
        private MockHttpMessageHandler _httpMessageHandler;
        private HttpClient _httpClient;
        
        public StiplyClientTest()
        { }

        [TestInitialize]
        public void init()
        {
            _httpMessageHandler = new MockHttpMessageHandler();
            _httpClient = new HttpClient(_httpMessageHandler)
            {
                BaseAddress = new Uri("http://api.stiply.test"),
            };
        }

        [TestMethod]
        public async Task Ping_ReturnSuccesValue_Succeeded()
        {
            var expectedStatusCode = 200;
            var expectedPingValue = "pong";
            var expectedToken = "Bearer supersecrettoken";

            // Arrange
            var jsonResult = JsonConvert.SerializeObject(new { status_code = expectedStatusCode, ping_value = expectedPingValue });
            _httpMessageHandler
                .When(HttpMethod.Post, "/v1.1/actions/ping")
                .WithHeaders("Authorization", expectedToken)
                .Respond(HttpStatusCode.OK, "application/json", jsonResult);

            // Act 
            var target = new StiplyClient(_httpClient);
            var actual = await target.Ping("ping", expectedToken);

            // Assert
            Assert.AreEqual(expectedStatusCode, actual.StatusCode);
            Assert.AreEqual(expectedPingValue, actual.PingValue);
        }

        [TestMethod]
        public async Task CreateSignRequest_ShouldReturnCreatedSignRequest()
        {
            var expectedFilename = "document.pdf";
            var expectedKey = "kYNtRHBaoB2VLiL34au0Hh8RWbeoLbUW7HdAXsfdldJnQh9epH";
            var expectedToken = "Bearer supersecrettoken";

            //Arrange
            _httpMessageHandler
                .When(HttpMethod.Post, "/v1.1/sign_requests")
                .WithHeaders("Authorization", expectedToken)
                .RespondWithJsonFile($"Results{Path.DirectorySeparatorChar}create-signrequest-response.json");

            // Act 
            byte[] fileBytes = File.ReadAllBytes("document.pdf");
            var dto = new CreateSignRequest()
            {
                term = "1w",
                fileName = "filename.pdf",
                file = fileBytes
            };

            var target = new StiplyClient(_httpClient);
            var actual = await target.CreateSignRequest(dto, expectedToken);

            // Assert
            Assert.IsNotNull(actual.Data);
            Assert.IsNotNull(actual.Data.Document);
            Assert.IsNotNull(actual.Data.SignRequest);

            Assert.AreEqual(expectedFilename, actual.Data.Document.Filename);
            Assert.AreEqual(expectedKey, actual.Data.SignRequest.Key);
        }

        [TestMethod]
        public async Task CreateSigner_ShouldReturnCreatedSigner()
        {
            var expectedToken = "Bearer supersecrettoken";
            var expectedKey = "kYNtRHBaoB2VLiL34au0Hh8RWbeoLbUW7HdAXsfdldJnQh9epH";
            var expectedEmail = "someone@somedomain.nl";
            var expectedSignerKey = "nMlT7NWokcQATAEIEDSrwKVKx6oX8jtnIOQrrZrT33ZGk6nVwC";

            //Arrange
            _httpMessageHandler
                .When(HttpMethod.Post, $"/v1.1/sign_requests/{expectedKey}/signers")
                .WithHeaders("Authorization", expectedToken)
                .RespondWithJsonFile($"Results{Path.DirectorySeparatorChar}create-signer-response.json");

            // Act 
            var dto = new CreateSigner()
            {
                signerEmail = expectedEmail,
                signerSignatureFields = new List<SignerSignatureField>
                {
                    new SignerSignatureField { Name = "signature_0" }
                },
                invitationMethod = "custom"
            };

            var target = new StiplyClient(_httpClient);
            var actual = await target.CreateSigner(dto, expectedKey, expectedToken);

            // Assert
            Assert.IsNotNull(actual.Data);
            
            Assert.AreEqual(expectedSignerKey, actual.Data.Key);
            Assert.AreEqual(expectedEmail, actual.Data.Email);
        }

        [TestMethod]
        public async Task SendSignRequest_ShouldReturnSentSignRequest()
        {
            var expectedToken = "Bearer supersecrettoken";
            var expectedKey = "kYNtRHBaoB2VLiL34au0Hh8RWbeoLbUW7HdAXsfdldJnQh9epH";
            var expectedFilename = "document.pdf";

            //Arrange
            _httpMessageHandler
                .When(HttpMethod.Post, $"/v1.1/sign_requests/{expectedKey}/actions/send")
                .WithHeaders("Authorization", expectedToken)
                .RespondWithJsonFile($"Results{Path.DirectorySeparatorChar}send-signrequest-response.json");

            // Act 
            var target = new StiplyClient(_httpClient);
            var actual = await target.SendSignRequest(expectedKey, expectedToken);

            // Assert
            Assert.IsNotNull(actual.Data);
            Assert.IsNotNull(actual.Data.Document);
            Assert.IsNotNull(actual.Data.SignRequest);

            Assert.AreEqual(expectedFilename, actual.Data.Document.Filename);
            Assert.AreEqual(expectedKey, actual.Data.SignRequest.Key);
            Assert.AreEqual(1, actual.Data.SignRequest.Sent);
        }

        [TestMethod]
        public async Task GetSignRequest_ShouldReturnCreatedSignRequest()
        {
            var expectedToken = "Bearer supersecrettoken";
            var expectedKey = "kYNtRHBaoB2VLiL34au0Hh8RWbeoLbUW7HdAXsfdldJnQh9epH";
            var expectedFilename = "document.pdf";
            var expectedIsSent = 1;
            var expectedNoOfSigners = 1;
            var expectedSignerKey = "nMlT7NWokcQATAEIEDSrwKVKx6oX8jtnIOQrrZrT33ZGk6nVwC";
            var expectedEmail = "someone@somedomain.nl";

            //Arrange
            _httpMessageHandler
                .When(HttpMethod.Get, $"/v1.1/sign_requests/{expectedKey}")
                .WithHeaders("Authorization", expectedToken)
                .RespondWithJsonFile($"Results{Path.DirectorySeparatorChar}get-signrequest-response.json");

            // Act 
            var target = new StiplyClient(_httpClient);
            var actual = await target.GetSignRequest(expectedKey, expectedToken);

            // Assert
            Assert.IsNotNull(actual.Data);
            Assert.IsNotNull(actual.Data.Document);
            Assert.IsNotNull(actual.Data.SignRequest);

            Assert.AreEqual(expectedFilename, actual.Data.Document.Filename);
            Assert.AreEqual(expectedKey, actual.Data.SignRequest.Key);
            Assert.AreEqual(expectedIsSent, actual.Data.SignRequest.Sent);

            Assert.AreEqual(expectedNoOfSigners, actual.Data.SignRequest.Signers.Count);
            Assert.AreEqual(expectedSignerKey, actual.Data.SignRequest.Signers[0].Key);
            Assert.AreEqual(expectedEmail, actual.Data.SignRequest.Signers[0].Email);
        }

        [TestMethod]
        public async Task GetSignedDocument_ShouldReturnSignedDocumentResponse()
        {
            var expectedToken = "Bearer supersecrettoken";
            var expectedKey = "kYNtRHBaoB2VLiL34au0Hh8RWbeoLbUW7HdAXsfdldJnQh9epH";
            var expectedFilename = "filename-[Ondertekend].pdf";
            var expectedMimeType = "application/pdf";
            var expectedHash = "d525419abd50b8e5113aca40982b37319b946b3d";

            //Arrange
            _httpMessageHandler
                .When(HttpMethod.Get, $"/v1.1/sign_requests/{expectedKey}/documents/actions/get_signed_document")
                .WithHeaders("Authorization", expectedToken)
                .RespondWithJsonFile($"Results{Path.DirectorySeparatorChar}get-signed-document-response.json");

            // Act
            var target = new StiplyClient(_httpClient);
            var actual = await target.GetSignedDocument(expectedKey, expectedToken);

            // Assert
            Assert.IsNotNull(actual.Data);
            
            Assert.AreEqual(expectedFilename, actual.FileName);
            Assert.AreEqual(expectedMimeType, actual.MimeType);
            Assert.AreEqual(expectedHash, actual.Hash);
        }

        [TestMethod]
        public async Task GetProofDocument_ShouldReturnProofDocumentResponse()
        {
            var expectedToken = "Bearer supersecrettoken";
            var expectedKey = "kYNtRHBaoB2VLiL34au0Hh8RWbeoLbUW7HdAXsfdldJnQh9epH";
            var expectedFilename = "filename-[Bewijs].pdf";
            var expectedMimeType = "application/pdf";
            var expectedHash = "903f0d342c9264088c625c2cd68e37d35b1d7b2b";

            //Arrange
            _httpMessageHandler
                .When(HttpMethod.Get, $"/v1.1/sign_requests/{expectedKey}/documents/actions/get_signed_document")
                .WithHeaders("Authorization", expectedToken)
                .RespondWithJsonFile($"Results{Path.DirectorySeparatorChar}get-proof-document-response.json");

            // Act 
            var target = new StiplyClient(_httpClient);
            var actual = await target.GetSignedDocument(expectedKey, expectedToken);

            // Assert
            Assert.IsNotNull(actual.Data);

            Assert.AreEqual(expectedFilename, actual.FileName);
            Assert.AreEqual(expectedMimeType, actual.MimeType);
            Assert.AreEqual(expectedHash, actual.Hash);
        }

        [TestMethod]
        public async Task ExtendTerm_ShouldReturnUpdatedSignRequest()
        {
            var expectedToken = "Bearer supersecrettoken";
            var expectedKey = "kYNtRHBaoB2VLiL34au0Hh8RWbeoLbUW7HdAXsfdldJnQh9epH";
            var expectedFilename = "document.pdf";
            var expectedIsSent = 1;
            var expectedNoOfSigners = 1;
            var expectedSignerKey = "nMlT7NWokcQATAEIEDSrwKVKx6oX8jtnIOQrrZrT33ZGk6nVwC";
            var expectedEmail = "someone@somedomain.nl";
            var expectedTerm = "3w";

            //Arrange
            _httpMessageHandler
                .When(HttpMethod.Post, $"/v1.1/sign_requests/{expectedKey}/actions/extend_term")
                .WithHeaders("Authorization", expectedToken)
                .RespondWithJsonFile($"Results{Path.DirectorySeparatorChar}extend-term-response.json");

            // Act 
            var target = new StiplyClient(_httpClient);
            var actual = await target.ExtendTerm(expectedKey, expectedTerm, expectedToken);

            // Assert
            Assert.IsNotNull(actual.Data);
            Assert.IsNotNull(actual.Data.Document);
            Assert.IsNotNull(actual.Data.SignRequest);

            var actualTerm = actual.Data.SignRequest.Terms.LastOrDefault();
            Assert.IsNotNull(actualTerm);
            Assert.AreEqual(expectedTerm, actualTerm.TermCode);

            Assert.AreEqual(expectedFilename, actual.Data.Document.Filename);
            Assert.AreEqual(expectedKey, actual.Data.SignRequest.Key);
            Assert.AreEqual(expectedIsSent, actual.Data.SignRequest.Sent);

            Assert.AreEqual(expectedNoOfSigners, actual.Data.SignRequest.Signers.Count);
            Assert.AreEqual(expectedSignerKey, actual.Data.SignRequest.Signers[0].Key);
            Assert.AreEqual(expectedEmail, actual.Data.SignRequest.Signers[0].Email);
        }
    }
}