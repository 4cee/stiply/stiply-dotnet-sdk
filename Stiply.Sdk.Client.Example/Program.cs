﻿using IdentityModel.OidcClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace Stiply.Sdk.Client.Example
{
    class Program
    {
        private static StiplyClient _stiplyClient;
        private static LoginResult _loginResult;
        private static string BasicToken => $"Bearer {_loginResult.AccessToken}";

        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            _loginResult = await LoginAsync();
            if (!String.IsNullOrEmpty(_loginResult.Error))
            {
                Console.WriteLine($"Could not authenticate: {_loginResult.Error}:\r\n{_loginResult.ErrorDescription}");
                return;
            }

            Console.WriteLine("Access token received.");

            var httpClientHandler = new HttpClientHandler();
#if(DEBUG)
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true; // DEBUGGING ONLY
#endif

            HttpClient client = new HttpClient(httpClientHandler)
            {
                BaseAddress = Config.StiplyApiUrl,
            };
            _stiplyClient = new StiplyClient(client);

            PingResponse pingResponse = await PingIt();
            Console.WriteLine("Hello {0} status {1}", pingResponse.PingValue, pingResponse.StatusCode);
            
            CreatedSignRequest signrequest = await Createsignrequest();
            string signRequestKey = signrequest.Data.SignRequest.Key;
            Console.WriteLine("Document {0} with SR Key {1}", signrequest.Data.Document.Filename, signRequestKey);
            
            CreatedSigner signer1 = await CreateSigner(signrequest.Data.SignRequest.Key, Config.Signer1Email, 0, Config.Signer1Name);
            Console.WriteLine("Signer {0} with email {1}", signer1.Data.Key, signer1.Data.Email);

            CreatedSigner signer2 = await CreateSigner(signrequest.Data.SignRequest.Key, Config.Signer2Email, 1);
            Console.WriteLine("Signer {0} with email {1}", signer2.Data.Key, signer2.Data.Email);

            SentSignRequest sendRequest = await SendSignRequest(signrequest.Data.SignRequest.Key);
            Console.WriteLine("Send SR {0} with name: {1}", sendRequest.Data.SignRequest.Key, sendRequest.Data.Document.Filename);

            await ExtendTerm(signRequestKey);

            CreatedSignRequest signRequestCopy = await GetSignRequest(signRequestKey);
            Console.WriteLine("Opening browser to {0} to sign the document.", signRequestCopy.Data.SignRequest.Signers[0].SignUrl);
            SystemBrowser.OpenBrowser(signRequestCopy.Data.SignRequest.Signers[0].SignUrl);


            Console.WriteLine("Waiting for sign request to complete.");
            await WaitForSignRequestCompleted(signRequestKey);
            
            DocumentResponse documentResponse = await GetSignedDocument(signRequestKey);
            await File.WriteAllBytesAsync(documentResponse.FileName, documentResponse.Data);
            Console.WriteLine("Document with mime {0} and hash  {1} with name  {2}", documentResponse.MimeType, documentResponse.Hash, documentResponse.FileName);
            OpenPdf(documentResponse.FileName);
            
            DocumentResponse proofDocumentResponse = await GetProofDocument(signRequestKey);
            await File.WriteAllBytesAsync(proofDocumentResponse.FileName, proofDocumentResponse.Data);            
            OpenPdf(proofDocumentResponse.FileName);
        }

        private static async Task<LoginResult> LoginAsync()
        {
            var options = new StiplyOAuthOptions
            {
                OAuthEndpointUrl = Config.StiplyOAuthUrl,
                ClientId = Config.StiplyClientId,
                ClientSecret = Config.StiplyClientSecret,
                LocalPort = Config.LocalPort,
            };

            var authenticator = new StiplyOAuthAuthenticator(options);
            return await authenticator.LoginAsync();
        }

        private static async Task<PingResponse> PingIt()
        {
            PingResponse response = await _stiplyClient.Ping("ping", BasicToken);
            return response;
        }

        private static async Task<CreatedSignRequest> Createsignrequest()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"../../../document.pdf");

            byte[] fileBytes = File.ReadAllBytes(path);
            var dto = new CreateSignRequest()
            {
                term = "1w",
                fileName = "filename.pdf",
                file = fileBytes,
            };
            CreatedSignRequest response = await _stiplyClient.CreateSignRequest(dto, BasicToken);
            return response;
        }

        private static async Task<CreatedSigner> CreateSigner(string key, string email, int index, string name = null)
        {
            var dto = new CreateSigner()
            {
                signerEmail = email,
                signerName = name,
                signerSignatureFields = new List<SignerSignatureField>()
                {
                    new SignerSignatureField { Name = $"signature_{index}" },
                },
                signerTextFields = new List<SignerSignatureField>
                {
                    new SignerSignatureField { Name = $"text_{index}" },
                },
                signerDateFields = new List<SignerSignatureField>
                {
                    new SignerSignatureField { Name = $"date_{index}" },
                },
                signerCheckboxFields = new List<SignerSignatureField>
                {
                    new SignerSignatureField { Name = $"c_{index}" },
                },
                signerInitialFields = new List<SignerSignatureField>
                {
                    new SignerSignatureField { Name = $"initial_{index}" },
                },
                invitationMethod = "custom"
            };
            CreatedSigner response = await _stiplyClient.CreateSigner(dto, key, BasicToken);
            return response;
        }

        private static async Task<ExtendedTerm> ExtendTerm(string key)
        {
            ExtendedTerm response = await _stiplyClient.ExtendTerm(key, "2w", BasicToken);
            return response;
        }

        private static async Task<SendReminderResponse> Reminder(string key)
        {
            SendReminderResponse response = await _stiplyClient.SendReminder(key, BasicToken);
            return response;
        }

        private static async Task<SentSignRequest> SendSignRequest(string key)
        {
            SentSignRequest response = await _stiplyClient.SendSignRequest(key, BasicToken);
            return response;
        }

        private static async Task<DocumentResponse> GetSignedDocument(string key)
        {
            DocumentResponse response = await _stiplyClient.GetSignedDocument(key, BasicToken);
            return response;
        }

        private static async Task<DocumentResponse> GetProofDocument(string key)
        {
            DocumentResponse response = await _stiplyClient.GetProofDocument(key, BasicToken);
            return response;
        }

        private static async Task<CreatedSignRequest> GetSignRequest(string key)
        {
            CreatedSignRequest response = await _stiplyClient.GetSignRequest(key, BasicToken);
            return response;
        }

        private static async Task WaitForSignRequestCompleted(string signRequestKey)
        {
            bool isCompleted = false;
            while (!isCompleted)
            {
                await Task.Delay(10000);
                CreatedSignRequest signRequest = await GetSignRequest(signRequestKey);
                if(signRequest.Data.SignRequest.AllSigned == 1)
                {
                    return;
                }
            }
        }

        private static void OpenPdf(string filename)
        {
            var absoluteFileNamePath = filename;
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("cmd", $"/c start {absoluteFileNamePath}") { CreateNoWindow = true });
        }
    }
}
