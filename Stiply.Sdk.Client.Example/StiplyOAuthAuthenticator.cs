﻿using IdentityModel.OidcClient;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Stiply.Sdk.Client.Example
{
    public class StiplyOAuthAuthenticator
    {
        private readonly StiplyOAuthOptions options;

        public StiplyOAuthAuthenticator(StiplyOAuthOptions options)
        {
            this.options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public async Task<LoginResult> LoginAsync()
        {
            var options = this.CreateOAuthClientOptions();
            var client = new OidcClient(options);
            var result = await client.LoginAsync(new LoginRequest());
            return result;
        }

        private OidcClientOptions CreateOAuthClientOptions()
        {
            var browser = new SystemBrowser(this.options.LocalPort);
            string redirectUri = string.Format($"http://localhost:{browser.Port}/");

            var httpClientHandler = new HttpClientHandler();
#if(DEBUG)
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
#endif
            var options = new OidcClientOptions
            {
                ProviderInformation = new ProviderInformation
                {
                    AuthorizeEndpoint = $"{this.options.OAuthEndpointUrl}/authorize",
                    TokenEndpoint = $"{this.options.OAuthEndpointUrl}/token",
                    IssuerName = this.options.OAuthEndpointUrl.ToString(),
                    KeySet = new IdentityModel.Jwk.JsonWebKeySet("{\"alg\":\"RS256\"}"),
                },
                Policy = new Policy
                {
                    RequireIdentityTokenSignature = false,
                    RequireIdentityTokenOnRefreshTokenResponse = false,

                },
                RedirectUri = redirectUri,
                ClientId = this.options.ClientId,
                ClientSecret = this.options.ClientSecret,
                FilterClaims = false,
                Browser = browser,
                BackchannelHandler = httpClientHandler,
                LoadProfile = false,
            };

            return options;
        }
    }
}
