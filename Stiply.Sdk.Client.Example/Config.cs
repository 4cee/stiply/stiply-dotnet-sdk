﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stiply.Sdk.Client.Example
{
    public static class Config
    {
        public static readonly Uri StiplyApiUrl = new Uri("https://api.stiply.nl");
        public static readonly Uri StiplyOAuthUrl = new Uri("https://app.stiply.nl/oauth");
        public static readonly string StiplyClientId = "{YOUR STIPLY CLIENT ID}";
        public static readonly string StiplyClientSecret = "{YOUR STIPLY CLIENT SECRET}";

        public static readonly int LocalPort = 44347; // Register the client with Redirect URL: 'http://localhost:44347/'
        public static readonly string Signer1Email = "johndoe@example.com";
        public static readonly string Signer1Name = "John Doe";
        public static readonly string Signer2Email = "janedoe@example.com";
        public static readonly string Signer2Name = "Jane Doe";
    }
}
