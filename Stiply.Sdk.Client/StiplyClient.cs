﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using System.Net.Http;
using System.Threading.Tasks;

namespace Stiply.Sdk
{
    /// <summary>
    /// The Stiply client that can be used to connect with the Stiply API
    /// </summary>
    public class StiplyClient : IStiplyClient
    {
        private readonly IStiplyRestClient _stiplyClient;

        /// <summary>
        /// </summary>
        /// <param name="client"></param>
        public StiplyClient(HttpClient client)
        {
            _stiplyClient = RestService.For<IStiplyRestClient>(client, new RefitSettings(new NewtonsoftJsonContentSerializer(new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })));
        }
        /// <summary>
        /// Test the API connections and authentication header
        /// </summary>
        /// <param name="ping"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<PingResponse> Ping(string ping, string token)
        {

            try
            {
                var body = new PingRequest
                {
                    ping_value = ping
                };
                return await _stiplyClient.Ping(body, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }


        }

        /// <summary>
        /// Create new signature request.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CreatedSignRequest> CreateSignRequest(CreateSignRequest model, string token)
        {
            try
            {
                var file = new ByteArrayPart(model.file, model.fileName);
                return await _stiplyClient.SignRequest(
                    token,
                    file,
                    model.term,
                    model.docName,
                    model.message,
                    model.comment,
                    model.externalKey,
                    model.callBackUrl
                );
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Add signer to sign request. This url may be called multiple times on one sign request to add multiple signers to the sign request.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CreatedSigner> CreateSigner(CreateSigner model, string signRequestKey, string token)
        {
            try
            {
                return await _stiplyClient.CreateSigner(model, signRequestKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Send sign request to first signer
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<SentSignRequest> SendSignRequest(string signRequestKey, string token)
        {
            try
            {
                return await _stiplyClient.SendSignRequest(signRequestKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Get signed document of sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<DocumentResponse> GetSignedDocument(string signRequestKey, string token)
        {
            try
            {
                return await _stiplyClient.GetSignedDocument(signRequestKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Get proof document of sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<DocumentResponse> GetProofDocument(string signRequestKey, string token)
        {
            try
            {
                return await _stiplyClient.GetProofDocument(signRequestKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Send a reminder e-mail to the current signer of the sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<SendReminderResponse> SendReminder(string signRequestKey, string token)
        {
            try
            {
                return await _stiplyClient.SendReminder(signRequestKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Extend the term of an existing sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="term"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ExtendedTerm> ExtendTerm(string signRequestKey, string term, string token)
        {
            try
            {
                return await _stiplyClient.ExtendTerm(signRequestKey, new ExtendTermRequest { Term = term }, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Get sign request key from external key (your local key).
        /// </summary>
        /// <param name="externalKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<SignRequestKey> GetSignRequestByExternalKey(string externalKey, string token)
        {
            try
            {
                return await _stiplyClient.GetSignRequestByExternalKey(externalKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Get a specific sign request
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CreatedSignRequest> GetSignRequest(string signRequestKey, string token)
        {
            try
            {
                return await _stiplyClient.GetSignRequest(signRequestKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Delete a specific sign request
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<DeleteResponse> DeleteSignRequest(string signRequestKey, string token)
        {
            try
            {
                return await _stiplyClient.DeleteSignRequest(signRequestKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Get a specific signer
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<SignerResponse> GetSigner(string signRequestKey, string signerKey, string token)
        {
            try
            {
                return await _stiplyClient.GetSigner(signRequestKey, signerKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Update the e-mail address and/or telephone number of a signer who has not yet signed. It is required to post both values, email and phone, when updating. Should you only want to update one of these, just submit the original value of the other.
        /// Please note that in case a signer has already received the sign request email and you update the email address of the signer afterwards, the link in the signer’s e-mail will not work anymore.The signer shall not receive a new sign request e-mail on the new e-mail address.Please use Send Reminder for that purpose after the e-mail address has been updated.If you only update the phone number the original sign link still works so no reminder is required.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="newEmail"></param>
        /// <param name="newPhoneNumber"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<UpdatedSigner> UpdateSigner(string signRequestKey, string signerKey, string newEmail, string newPhoneNumber, string token)
        {
            try
            {
                var updateSigner = new UpdateSignerRequest() { NewCellPhoneNumber = newPhoneNumber, NewEmail = newEmail };
                return await _stiplyClient.UpdateSigner(signRequestKey, signerKey, updateSigner, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Delete a specific signer
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<DeleteResponse> DeleteSigner(string signRequestKey, string signerKey, string token)
        {
            try
            {
                return await _stiplyClient.DeleteSigner(signRequestKey, signerKey, token);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
        }

    }
}
