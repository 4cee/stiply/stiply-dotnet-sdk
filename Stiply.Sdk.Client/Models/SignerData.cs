using Newtonsoft.Json;
using System.Collections.Generic;

namespace Stiply.Sdk
{
    /// <summary>
    /// The data response object of a created signer.
    /// </summary>
    public partial class SignerData
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [JsonProperty("key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the email address
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public dynamic Name { get; set; }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        [JsonProperty("order")]
        public long? Order { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        [JsonProperty("language")]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the phone no.
        /// </summary>
        [JsonProperty("phone")]
        public dynamic Phone { get; set; }

        /// <summary>
        /// Gets or sets the authentication method.
        /// </summary>
        [JsonProperty("auth_method")]
        public dynamic AuthMethod { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        [JsonProperty("role")]
        public string Role { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [JsonProperty("message")]
        public dynamic Message { get; set; }

        /// <summary>
        /// Gets or sets the sign url.
        /// </summary>
        [JsonProperty("sign_url")]
        public dynamic SignUrl { get; set; }

        /// <summary>
        /// Gets or sets the emandate properties
        /// </summary>
        [JsonProperty("emandate")]
        public dynamic Emandate { get; set; }

        /// <summary>
        /// Gets or sets the moment the signer is created.
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the moment the signer is last updated.
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets the signer progress.
        /// </summary>
        [JsonProperty("signer_progresses")]
        public List<dynamic> SignerProgresses { get; set; }

        /// <summary>
        /// Gets or sets the signer fields.
        /// </summary>
        [JsonProperty("fields")]
        public List<Field> Fields { get; set; }
    }

}