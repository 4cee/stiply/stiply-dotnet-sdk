using Newtonsoft.Json;
using System.Collections.Generic;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response of GetSigner.
    /// </summary>
    public partial class SignerResponse
    {
        /// <summary>
        /// The Data response object
        /// </summary>
        [JsonProperty("data")]
        public SignerData Data { get; set; }
    }
}