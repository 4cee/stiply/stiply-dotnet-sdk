using Newtonsoft.Json;
using System.Collections.Generic;

namespace Stiply.Sdk
{
    /// <summary>
    /// DTO for the Create Signer Request
    /// </summary>
    public class CreateSigner
    {
        /// <summary>
        /// Authentication method for signer. Currently sms, emandate and idin; are available as a valid option.
        /// </summary>
        /// <value>sms, emandate, idin, pimid</value>
        [JsonProperty("auth_method", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? authMethod { get; set; }

        /// <summary>
        /// ellular phone number of the signer is required in case auth_method is set to sms. Without setting auth_method to sms the phone number will not be saved.
        /// </summary>
        /// <value>Phone numbers need to contain a country code as follows: 31655136642</value>
        [JsonProperty("cell_phone_number", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? cellPhoneNumber { get; set; }

        /// <summary>
        /// You may add an optional emandate object, when auth_method is set to emandate. The emandate object has three parameters:
        /// instrument_code: Either core or b2b, core being for consumer emandates and b2b for business to business emandates.
        /// sequence_type: Either rcur or ooff, rcur being for a recurring emandate and ooff for a one-off emandate.
        /// emandate_id (optional):  Your own emandate identifier. Is not set, Stiply will generate an emandate id for you.
        /// </summary>
        /// <example>
        /// {"signer_email":"signer@stiply.nl","signer_signature_fields":[{"name":"signature1"}], "auth_method":"emandate","emandate":{"instrument_code":"core", "sequence_type":"rcur","emandate_id":"12345"}}
        /// </example>
        [JsonProperty("emandate", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public Emandate? emandate { get; set; }

        /// <summary>
        /// You may add an optional idin object, when auth_method is set to idin. The idin object has three parameters:
        /// 
        ///  - prefix: The prefix of the name of the signer, for example if the signers name is Mike van der Meer, van der would be the prefix.
        /// 
        ///  - last_name: The lastname of the signer, in the above example this would be Meer.
        /// 
        ///  - date_of_birth: provide the date of birth of the signer here in the following format: dd-mm-yyyy, so for example 01-09-1982
        /// 
        /// </summary>
        [JsonProperty("idin", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public Idin? idin { get; set; }

        /// <summary>
        /// If you would like to invite the signer yourself, you should set the invitation_method to custom. In that case Stiply shall not send an email with the sign request to the signer. Instead, when you send the sign request, the API shall return a unique sign link to you, which you can provide to the signer yourself.
        /// </summary>
        [JsonProperty("invitation_method", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? invitationMethod { get; set; }

        /// <summary>
        /// The language in which the signer receives correspondence. Current options are nl, de, es, en, da, pl, it, fr, sv. If no language is provided, the account language of sender will be used.
        /// </summary>
        [JsonProperty("language", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? language { get; set; }

        /// <summary>
        /// You can send a specific message to a signer, that overrules the sign_request message in the mail to the signer.
        /// </summary>
        /// <value></value>
        [JsonProperty("message", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? message { get; set; }

        /// <summary>
        /// You may add an optional pimid object, when auth_method is set to pimid. The pimid object has three parameters:
        /// 
        ///  - initials: The initials of the name of the signer.
        /// 
        ///  - family_name: The family name of the signer.
        /// 
        ///  - date_of_birth: provide the date of birth of the signer here in the following format: dd-mm-yyyy, so for example 01-09-1982
        /// 
        /// </summary>
        [JsonProperty("pimid", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public PimId? pimId { get; set; }

        /// <summary>
        /// An optional url to redirect the signer to when the signer completes the sign request.
        /// </summary>
        [JsonProperty("redirect_url", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string redirectUrl { get; set; }

        /// <summary>
        /// A signer can have different roles. When default, the signer will need to sign the document regularly. When the value cc is set as role, the signer shall be copy-only (cc).
        /// </summary>
        [JsonProperty("role", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? role { get; set; }

        /// <summary>
        /// The e-mail address of signer
        /// </summary>
        /// <value>email</value>
        [JsonProperty("signer_email", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string signerEmail { get; set; }

        /// <summary>
        /// The name of the signer. This value is optional and will only be used to prefill the signers name in the viewer.
        /// </summary>
        [JsonProperty("signer_name", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? signerName { get; set; }

        /// <summary>
        /// Checkbox fields can be added in the same ways as signature fields: with a tag or with coordinates. The maximum values for the X and Y coordinates are: X-max: 524 Y-max: 1060 The top left of the field shall be positioned on the top left of the tag, provided that the field does not cross the page boundaries. You can optionally specify the width of the field, also provided that you keep within the minimum and maximum field dimensions.
        /// </summary>
        /// <example>
        ///  <code>
        /// Example: 
        ///     "signer_checkbox_fields"[{"name":"date_field_1","width":"150"}]
        ///      or
        ///     "signer_checkbox_fields"[{"page":1,"x":300,"y":300}, {"page":2,"x":400,"y":400}]
        /// </code>
        /// </example>

        [JsonProperty("signer_checkbox_fields", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public List<SignerSignatureField>? signerCheckboxFields { get; set; }

        /// <summary>
        /// Date fields can be added in the same ways as signature fields: with a tag or with coordinates. The maximum values for the X and Y coordinates are: X-max: 524 Y-max: 1060 The top left of the field shall be positioned on the top left of the tag, provided that the field does not cross the page boundaries. You can optionally specify the width of the field, also provided that you keep within the minimum and maximum field dimensions.
        /// </summary>
        /// <example>
        ///  <code>
        /// Example: 
        ///     "signer_date_fields"[{"name":"date_field_1","width":"150"}]
        ///      or
        ///     "signer_date_fields"[{"page":1,"x":300,"y":300}, {"page":2,"x":400,"y":400}]
        /// </code>
        /// </example>

        [JsonProperty("signer_date_fields", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public List<SignerSignatureField>? signerDateFields { get; set; }

        /// <summary>
        /// Intiials fields can be added in the same ways as signature fields: with a tag or with coordinates. The maximum values for the X and Y coordinates are: X-max: 524 Y-max: 1060 The top left of the field shall be positioned on the top left of the tag, provided that the field does not cross the page boundaries. You can optionally specify the width of the field, also provided that you keep within the minimum and maximum field dimensions.
        /// </summary>
        /// <example>
        ///  <code>
        /// Example: 
        ///     "signer_initial_fields"[{"name":"date_field_1","width":"150"}]
        ///      or
        ///     "signer_initial_fields"[{"page":1,"x":300,"y":300}, {"page":2,"x":400,"y":400}]
        /// </code>
        /// </example>

        [JsonProperty("signer_initial_fields", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public List<SignerSignatureField>? signerInitialFields { get; set; }

        /// <summary>
        /// Array with arrays containing signature field information, by using either a tag or coordinates.
        /// 
        /// Tags:
        /// Make sure the document includes a tag where the signature field should be added. You can pick the name yourself, but the tag has to be surrounded by accolades, like so: {{signature1}}. Please make sure the tag is not connected to another word in the document. Your JSON should look something like this: "signer_signature_fields":[{"name" : "signature1"}] The top left of the field shall be positioned on the top left of the tag, provided that the field does not cross the page boundaries. You can optionally specify the width of the field, also provided that you keep within the minimum and maximum field dimensions.
        /// 
        /// Coordinates:
        /// To add a field by coordinates you have to specify the page, x-coordinate and y-coordinate of the field. Coordinates are in pixels, relative to the manager viewport. (0,0) is the top left corner. You can check the coordinates by logging in the manager (app.stiply.nl/manager) as an API-user and adding a field to a document: the coordinates will be displayed in the field. X-max: 516 Y-max: 956
        /// </summary>
        /// <value></value>
        /// <example>
        ///  <code>
        ///   Tags:{"signer_email":"test@email.com", "signer_signature_fields":[{"name":"signature1","width":"300"}, {"name":"signature2"}]}
        /// 
        ///   Coordinates: {"signer_signature_fields":[{"page":1,"x":100,"y":200}]
        ///  </code>
        ///  </example>
        [JsonProperty("signer_signature_fields", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public List<SignerSignatureField> signerSignatureFields { get; set; }

        /// <summary>
        /// Text fields can be added in the same ways as signature fields: with a tag or with coordinates. The maximum values for the X and Y coordinates are: X-max: 524 Y-max: 1060 The top left of the field shall be positioned on the top left of the tag, provided that the field does not cross the page boundaries. You can optionally specify the width of the field, also provided that you keep within the minimum and maximum field dimensions.
        /// </summary>
        /// <example>
        ///  <code>
        /// Example: 
        ///     "signer_text_fields"[{"name":"text_field_1","width":"150"}]
        ///      or
        ///     "signer_text_fields"[{"page":1,"x":300,"y":300}, {"page":2,"x":400,"y":400}]
        /// </code>
        /// </example>

        [JsonProperty("signer_text_fields", Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public List<SignerSignatureField>? signerTextFields { get; set; }
    }
}