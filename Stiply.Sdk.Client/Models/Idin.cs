﻿using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// Idin authentication properties
    /// </summary>
    public class Idin
    {
        /// <summary>
        /// The prefix of the name of the signer, for example if the signers name is Mike van der Meer, van der would be the prefix.
        /// </summary>
        [JsonProperty("prefix", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Prefix { get; set; }

        /// <summary>
        /// The lastname of the signer, in the above example this would be Meer.
        /// </summary>
        [JsonProperty("last_name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }

        /// <summary>
        /// provide the date of birth of the signer here in the following format: dd-mm-yyyy, so for example 01-09-1982
        /// </summary>
        [JsonProperty("date_of_birth", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string DateOfBirth { get; set; }
    }
}
