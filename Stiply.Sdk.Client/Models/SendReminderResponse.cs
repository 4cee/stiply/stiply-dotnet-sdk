using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// Response of a sent reminder.
    /// </summary>
    public partial class SendReminderResponse
    {
        /// <summary>
        /// The data response object
        /// </summary>
        [JsonProperty("data")]
        public SignerData Data { get; set; }
    }


}