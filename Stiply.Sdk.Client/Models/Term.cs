using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// A term in a sign request.
    /// </summary>
    public partial class Term
    {
        /// <summary>
        /// 2 digit code representing the sign term (1d = 1 day, 2w = 2 weeks, 3m = 3 months)
        /// </summary>
        [JsonProperty("term_code")]
        public string TermCode { get; set; }

        /// <summary>
        /// Get or sets whether the term is expired.
        /// </summary>
        [JsonProperty("term_expired")]
        public long? TermExpired { get; set; }

        /// <summary>
        /// Gets or sets the moment the term expires on.
        /// </summary>
        [JsonProperty("term_expires")]
        public string TermExpires { get; set; }

        /// <summary>
        /// Gets or sets the moment the term is created.
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
    }
}