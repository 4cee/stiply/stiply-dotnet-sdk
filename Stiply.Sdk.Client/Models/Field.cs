using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// DTO for the field entity
    /// </summary>
    public partial class Field
    {
        /// <summary>
        /// Gets or sets the field type
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the X coordinate of the field
        /// </summary>
        [JsonProperty("x")]
        public long? X { get; set; }

        /// <summary>
        /// Gets or sets the Y coordinate of the field
        /// </summary>
        [JsonProperty("y")]
        public double? Y { get; set; }

        /// <summary>
        /// Gets or sets the width of the field
        /// </summary>
        [JsonProperty("width")]
        public long? Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the field
        /// </summary>
        [JsonProperty("height")]
        public long? Height { get; set; }

        /// <summary>
        /// Gets or sets the page the field is on
        /// </summary>
        [JsonProperty("page")]
        public long? Page { get; set; }

        /// <summary>
        /// Gets or sets the moment the field is created.
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the moment the field is last updated.
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }

}