using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The progress of a signer in the sign request.
    /// </summary>
    public partial class SignerProgress
    {
        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        [JsonProperty("action")]
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets the IP address of the action.
        /// </summary>
        [JsonProperty("ip")]
        public dynamic Ip { get; set; }

        /// <summary>
        /// Gets or sets the location of the action.
        /// </summary>
        [JsonProperty("location")]
        public dynamic Location { get; set; }

        /// <summary>
        /// Gets or sets the system of the action.
        /// </summary>
        [JsonProperty("system")]
        public dynamic System { get; set; }

        /// <summary>
        /// Get or sets the value of the action.
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }

        /// <summary>
        /// Get or sets the moment the action is created.
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the status of the action.
        /// </summary>
        [JsonProperty("status")]
        public dynamic Status { get; set; }
    }
}