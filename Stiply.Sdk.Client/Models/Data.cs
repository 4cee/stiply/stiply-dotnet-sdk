using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The Data response object of a sign request Response.
    /// </summary>
    public partial class Data
    {
        /// <summary>
        /// Document that needs to be signed
        /// </summary>
        [JsonProperty("document")]
        public Document Document { get; set; }

        /// <summary>
        /// Sign Request data
        /// </summary>
        [JsonProperty("sign_request")]
        public SignRequest SignRequest { get; set; }
    }
}