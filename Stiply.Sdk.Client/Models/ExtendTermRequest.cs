﻿using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The extend term request.
    /// </summary>
    public partial class ExtendTermRequest
    {
        /// <summary>
        /// 2 digit code representing the sign term (1d = 1 day, 2w = 2 weeks, 3m = 3 months)
        /// </summary>
        [JsonProperty("term", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Term { get; set; }
    }
}
