﻿using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The signature field of a signer.
    /// </summary>
    public class SignerSignatureField
    {
        /// <summary>
        /// Gets or sets the name of the signature field.
        /// </summary>
        [JsonProperty("name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the width of the field.
        /// </summary>
        [JsonProperty("width", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public long? Width { get; set; }

        /// <summary>
        /// Gets or sets the page the field is on.
        /// </summary>
        [JsonProperty("page", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public long? Page { get; set; }

        /// <summary>
        /// Gets or sets the X coordinate of the field.
        /// </summary>
        [JsonProperty("x", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public long? X { get; set; }

        /// <summary>
        /// Gets or sets the Y coordinate of the field.
        /// </summary>
        [JsonProperty("y", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public long? Y { get; set; }
    }
}
