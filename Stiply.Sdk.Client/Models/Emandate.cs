﻿using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// Emandate authentication properties
    /// </summary>
    public class Emandate
    {
        /// <summary>
        /// Either core or b2b, core being for consumer emandates and b2b for business to business emandates.
        /// </summary>
        [JsonProperty("instrument_code", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string InstrumentCode { get; set; }

        /// <summary>
        /// Either rcur or ooff, rcur being for a recurring emandate and ooff for a one-off emandate.
        /// </summary>
        [JsonProperty("sequence_type", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string SequenceType { get; set; }

        /// <summary>
        /// Your own emandate identifier.Is not set, Stiply will generate an emandate id for you.
        /// </summary>
        [JsonProperty("emandate_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string? EmandateId { get; set; }
    }
}
