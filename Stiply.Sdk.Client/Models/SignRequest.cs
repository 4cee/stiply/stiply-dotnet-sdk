using Newtonsoft.Json;
using System.Collections.Generic;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response properties of a sign request
    /// </summary>
    public partial class SignRequest
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [JsonProperty("key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets whether the request is sent.
        /// </summary>
        [JsonProperty("sent")]
        public long? Sent { get; set; }

        /// <summary>
        /// Gets or sets the moment the request is sent.
        /// </summary>
        [JsonProperty("sent_at")]
        public string SentAt { get; set; }

        /// <summary>
        /// Gets or sets whether all signers have signed the request.
        /// </summary>
        [JsonProperty("all_signed")]
        public long? AllSigned { get; set; }

        /// <summary>
        /// Gets or sets the moment all signers have signed the request.
        /// </summary>
        [JsonProperty("all_signed_at")]
        public string AllSignedAt { get; set; }

        /// <summary>
        /// Gets or sets the signed hash.
        /// </summary>
        [JsonProperty("signed_hash")]
        public dynamic SignedHash { get; set; }

        /// <summary>
        /// Gets or sets the proof hash.
        /// </summary>
        [JsonProperty("proof_hash")]
        public dynamic ProofHash { get; set; }

        /// <summary>
        /// Gets or sets whether the request is canceled.
        /// </summary>
        [JsonProperty("canceled")]
        public long? Canceled { get; set; }

        /// <summary>
        /// Gets or sets the moment de request is canceled.
        /// </summary>
        [JsonProperty("canceled_at")]
        public string CanceledAt { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [JsonProperty("comment")]
        public dynamic Comment { get; set; }

        /// <summary>
        /// Gets or sets the external reference key of the sign request.
        /// </summary>
        [JsonProperty("external_key")]
        public dynamic ExternalKey { get; set; }

        /// <summary>
        /// Gets or sets the callback url.
        /// </summary>
        [JsonProperty("call_back_url")]
        public dynamic CallBackUrl { get; set; }

        /// <summary>
        /// Gets or sets the moment the signrequest is created.
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the moment the signrequest is last updated.
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets the terms in the request.
        /// </summary>
        [JsonProperty("terms")]
        public List<Term> Terms { get; set; }

        /// <summary>
        /// Gets or sets the signers in the request.
        /// </summary>
        [JsonProperty("signers")]
        public List<Signer> Signers { get; set; }
    }
}