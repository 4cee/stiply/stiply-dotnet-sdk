﻿using Refit;
using System.Threading.Tasks;

namespace Stiply.Sdk
{
    /// <summary>
    /// The interface for the stiply client
    /// </summary>
    [Headers("application/json")]
    internal interface IStiplyRestClient
    {
        /// <summary>
        /// Test the API connections and authentication header
        /// </summary>
        /// <param name="ping"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [Post("/v1.1/actions/ping")]
        Task<PingResponse> Ping([Body] PingRequest ping, [Header("Authorization")] string token);

        /// <summary>
        /// Create new signature request.
        /// </summary>
        /// <returns>The signrequest</returns>
        [Multipart]
        [Post("/v1.1/sign_requests")]
        Task<CreatedSignRequest> SignRequest(
            [Header("Authorization")] string token,
            [AliasAs("file")] ByteArrayPart file,
            [AliasAs("term")] string term,
            [AliasAs("doc_name")] string docName,
            [AliasAs("message")] string message,
            [AliasAs("comment")] string comment,
            [AliasAs("external_key")] string externalKey,
            [AliasAs("call_back_url")] string callBackUrl);

        /// <summary>
        /// Add signer to sign request. This url may be called multiple times on one sign request to add multiple signers to the sign request.
        /// </summary>
        /// <param name="signer"></param>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [Post("/v1.1/sign_requests/{sign_request_key}/signers")]
        Task<CreatedSigner> CreateSigner([Body] CreateSigner signer, [AliasAs("sign_request_key")] string signRequestKey, [Header("Authorization")] string token);

        /// <summary>
        /// Send sign request to first signer
        /// </summary>
        /// <returns>the sign request with signers</returns>
        [Post("/v1.1/sign_requests/{sign_request_key}/actions/send")]
        Task<SentSignRequest> SendSignRequest([AliasAs("sign_request_key")] string signRequestKey, [Header("Authorization")] string token);

        /// <summary>
        /// Get signed document of sign request.
        /// </summary>
        /// <returns>The signed document</returns>
        [Get("/v1.1/sign_requests/{sign_request_key}/documents/actions/get_signed_document")]
        Task<DocumentResponse> GetSignedDocument([AliasAs("sign_request_key")] string signRequestKey, [Header("Authorization")] string token);

        /// <summary>
        /// Get proof document of sign request.
        /// </summary>
        /// <returns>The proof document</returns>
        [Get("/v1.1/sign_requests/{sign_request_key}/documents/actions/get_proof_document")]
        Task<DocumentResponse> GetProofDocument([AliasAs("sign_request_key")] string signRequestKey, [Header("Authorization")] string token);

        /// <summary>
        /// Send a reminder e-mail to the current signer of the sign request.
        /// </summary>
        /// <returns>The sign request with signers</returns>
        [Post("/v1.1/sign_requests/{sign_request_key}/actions/send_reminder")]
        Task<SendReminderResponse> SendReminder([AliasAs("sign_request_key")] string signRequestKey, [Header("Authorization")] string token);

        /// <summary>
        /// Extend the term of an existing sign request.
        /// </summary>
        /// <returns>The extended term</returns>
        [Post("/v1.1/sign_requests/{sign_request_key}/actions/extend_term")]
        Task<ExtendedTerm> ExtendTerm([AliasAs("sign_request_key")] string signRequestKey, [Body] ExtendTermRequest term, [Header("Authorization")] string token);

        /// <summary>
        /// Get sign request key from external key (your local key).
        /// </summary>
        /// <returns>The key of the sign request</returns>
        [Get("/v1.1/sign_requests/{your_external_key}/actions/get_sign_request_key")]
        Task<SignRequestKey> GetSignRequestByExternalKey([AliasAs("your_external_key")] string externalKey, [Header("Authorization")] string token);


        /// <summary>
        /// Get a specific sign request
        /// </summary>
        /// <returns>A sign request</returns>
        [Get("/v1.1/sign_requests/{sign_request_key}")]
        Task<CreatedSignRequest> GetSignRequest([AliasAs("sign_request_key")] string signRequestKey, [Header("Authorization")] string token);


        /// <summary>
        /// Delete a specific sign request
        /// </summary>
        /// <returns>Status response</returns>
        [Delete("/v1.1/sign_requests/{sign_request_key}")]
        Task<DeleteResponse> DeleteSignRequest([AliasAs("sign_request_key")] string signRequestKey, [Header("Authorization")] string token);

        /// <summary>
        /// Get a specific signer
        /// </summary>
        /// <returns>A signer</returns>
        [Get("/v1.1/sign_requests/{sign_request_key}/signers/{signer_key}")]
        Task<SignerResponse> GetSigner([AliasAs("sign_request_key")] string signRequestKey, [AliasAs("signer_key")] string signerKey, [Header("Authorization")] string token);

        /// <summary>
        /// Update the e-mail address and/or telephone number of a signer who has not yet signed. It is required to post both values, email and phone, when updating. Should you only want to update one of these, just submit the original value of the other.
        /// Please note that in case a signer has already received the sign request email and you update the email address of the signer afterwards, the link in the signer’s e-mail will not work anymore.The signer shall not receive a new sign request e-mail on the new e-mail address.Please use Send Reminder for that purpose after the e-mail address has been updated.If you only update the phone number the original sign link still works so no reminder is required.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="updateSigner"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [Put("/v1.1/sign_requests/{sign_request_key}/signers/{signer_key}")]
        Task<UpdatedSigner> UpdateSigner([AliasAs("sign_request_key")] string signRequestKey, [AliasAs("signer_key")] string signerKey, [Body] UpdateSignerRequest updateSigner, [Header("Authorization")] string token);

        /// <summary>
        /// Delete a specific signer
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [Delete("/v1.1/sign_requests/{sign_request_key}/signers/{signer_key}")]
        Task<DeleteResponse> DeleteSigner([AliasAs("sign_request_key")] string signRequestKey,
                                          [AliasAs("signer_key")] string signerKey,
                                          [Header("Authorization")] string token);

    }
}
